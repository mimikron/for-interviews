/* скрипт выводит полную информацию по регистрациям
позволяет проверить:  1) наличие разрешений на объект 2) наличие apple token */
SELECT DISTINCT os.id, os.obj_number as 'объект',
    op.remote_arming as 'ra',
   op.remote_disarming as 'rd',    
   pf.kts,    
   cs.id as 'контракт',
   cs.`date`,
   cs.id1c as 'id 1c контракта',
   os.address,
   cp.id_client,
   cp.id,
   cs.`number`,
   rg.id as 'рег',
   (SELECT COUNT(*) FROM obj_permissions WHERE obj_permissions.object_id = os.id) as 'разр',
   if(LENGTH(ls.apple_token)>0,'ЕСТЬ', '0') as APPLE, ph.phone as 'номер',
   rg.model_name as 'модель телефона',
   ps.fio as 'ФИО клиетна',
   if(os.name <> NULL, os.name, adreess_by_objnum(os.obj_number)) as 'адрес объекта',
   os.name as 'Имя объекта',
   rg.dateReg as 'дата регистрации',
   ls.token,
   ls.dateUpdate
FROM reg rg
LEFT JOIN login_sessions     ls  ON ls.registration_id = rg.id AND ls.is_deleted = 0
LEFT JOIN phones             ph  ON rg.phone_id = ph.id AND ph.isDeleted = 0
LEFT JOIN persons            ps  ON ps.id = ph.person_id
LEFT JOIN client_person_rels cp  ON cp.id_person = ps.id  AND cp.isDeleted = 0
LEFT JOIN contracts          cs  ON cs.client_id = cp.id_client AND cs.isDeleted = 0
LEFT JOIN obj_per_contracts  oj  ON cs.id = oj.id_contract
LEFT JOIN objects            os  ON os.id = oj.id_object AND os.isDeleted = 0
LEFT JOIN obj_permissions    op  ON cp.id = op.id_client_person_rel AND op.isDeleted = 0
LEFT JOIN preferences        pf  ON os.id = pf.object_id AND pf.registration_id = ls.registration_id
WHERE ph.phone = 8**********
GROUP BY os.obj_number,op.remote_arming,op.remote_disarming,pf.kts, cs.id, rg.id,ls.apple_token, os.name,cabinet.os.id,cp.id_client, ls.token, cp.id, ls.dateUpdate
ORDER BY rg.id DESC
