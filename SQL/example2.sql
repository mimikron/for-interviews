/* скрипт выводит метрики по постановкам за последние 3 дня*/

SELECT a1.date, IFNULL(a2.arm_with_delay, 0) as arm_with_delay, IFNULL(pin_wrong, 0) as pin_wrong, IFNULL(error_arm, 0) as error_arm, IFNULL(busy, 0) as busy,
IFNULL(no_hw, 0) as no_hw, IFNULL(allready, 0) as allready,IFNULL(a8.no_answ, 0) as 'no_answ', IFNULL(a9.all, 0) as 'all'
FROM (
SELECT DATE_ADD(CURRENT_DATE, INTERVAL - 0 DAY) as date
UNION
SELECT DATE_ADD(CURRENT_DATE, INTERVAL - 1 DAY)
UNION
SELECT DATE_ADD(CURRENT_DATE, INTERVAL - 2 DAY)) a1
LEFT JOIN (SELECT DATE(date_create) as date, COUNT(*) as 'arm_with_delay'
    FROM arming_disarming ad 
    JOIN iotserver.iot_answer ia on ia.task_id = ad.id_task 
    WHERE task_result LIKE '%task result:None%' AND (ia.`data` LIKE '%<code>-16</code>%' OR ia.`data` LIKE '%<code>-18</code>%') AND TO_DAYS(NOW()) - TO_DAYS(date_update) < 3
    GROUP BY DATE(date_create)
    ORDER BY DATE(date_create) DESC ) a2 on a2.date=a1.date
LEFT JOIN (SELECT DATE(dt) as date, COUNT(*) as 'pin_wrong' 
    FROM arming_disarming ad 
    JOIN iotserver.iot_answer ia on ia.task_id = ad.id_task 
    WHERE ia.`data` LIKE '%<code>-8</code>%' AND TO_DAYS(NOW()) - TO_DAYS(dt) < 3
    GROUP BY DATE(dt)
    ORDER BY DATE(dt) DESC ) a3 on a3.date=a1.date 
LEFT JOIN (SELECT DATE(dt) as date, COUNT(*) as 'error_arm' 
    FROM arming_disarming ad 
    JOIN iotserver.iot_answer ia on ia.task_id = ad.id_task 
    WHERE ia.`data` LIKE '%<code>-17</code>%' AND TO_DAYS(NOW()) - TO_DAYS(dt) < 3
    GROUP BY DATE(dt)
    ORDER BY DATE(dt) DESC ) a4 on a4.date=a1.date 
LEFT JOIN (SELECT DATE(dt) as date, COUNT(*) as 'busy' 
    FROM arming_disarming ad 
    JOIN iotserver.iot_answer ia on ia.task_id = ad.id_task 
    WHERE ia.`data` LIKE '%<code>-7</code>%' AND TO_DAYS(NOW()) - TO_DAYS(dt) < 3
    GROUP BY DATE(dt)
    ORDER BY DATE(dt) DESC ) a5 on a5.date=a1.date
LEFT JOIN (SELECT DATE(date_create) as date, COUNT(*) as 'no_hw' 
    FROM arming_disarming
    WHERE task_result LIKE '%task result:None%' AND hardware_id = 0 AND TO_DAYS(NOW()) - TO_DAYS(date_create) < 3
    GROUP BY DATE(date_create)
    ORDER BY DATE(date_create) DESC ) a6 on a6.date=a1.date
LEFT JOIN (SELECT DATE(dt) as date, COUNT(*) as 'allready' 
    FROM arming_disarming ad 
    JOIN iotserver.iot_answer ia on ia.task_id = ad.id_task 
    WHERE (ia.`data` LIKE '%<code>-14</code>%' OR ia.`data` LIKE '%<code>-15</code>%') AND TO_DAYS(NOW()) - TO_DAYS(dt) < 3
    GROUP BY DATE(dt)
    ORDER BY DATE(dt) DESC ) a7 on a7.date=a1.date 
LEFT JOIN (SELECT DATE(date_create) as date, COUNT(*) as 'no_answ' 
    FROM arming_disarming ad 
    WHERE ad.id_task NOT IN (SELECT task_id FROM iotserver.iot_answer ia WHERE TO_DAYS(NOW()) - TO_DAYS(ia.dt) < 3) AND TO_DAYS(NOW()) - TO_DAYS(date_create) < 3
    GROUP BY DATE(date_create)
    ORDER BY DATE(date_create) DESC ) a8 on a8.date=a1.date 
LEFT JOIN (SELECT DATE(date_create) as date, COUNT(*) as 'all' 
    FROM arming_disarming ad 
    WHERE TO_DAYS(NOW()) - TO_DAYS(date_create) < 3
    GROUP BY DATE(date_create)
    ORDER BY DATE(date_create) DESC ) a9 on a9.date=a1.date 
